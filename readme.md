``` sh
composer require symfony/maker-bundle --dev
composer require symfony/var-dumper --dev
composer require doctrine/annotations
composer require symfony/serializer-pack
composer require validator twig-bundle orm-pack security-csrf annotations
composer require symfony/webpack-encore-bundle
php bin\console make:controller ReportController

```
