<?php

namespace App\Controller;

use DateTime;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReportController extends AbstractController
{
    /**
     * @Route(name="report")
     */
    public function generate($data)
    {
        $year = $data['year'];
        $plainText = $data['plainText'];
        $language = $data['choiceLanguage'];

        if ($language) {
            ReportController::setLanguage($language);
        };

        $data = (object) array('months' => [], 'salary' => [], 'bonus' => []);

        // gen months
        ReportController::months($data);

        if ($plainText) {
            return ReportController::plainText($year, $data);
        } else {
            return ReportController::csv($year, $data);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function csv($year, $data)
    {
        $rows = [];

        // create titles 
        $rows[] = implode(',', array_keys((array) $data));

        // paste data into CSV
        $max = sizeof($data->months);
        for ($i = 0; $i < $max; $i++) {

            $dataForRow = [
                $data->months[$i],
                ReportController::salary($year, $i),
                ReportController::bonus($year, $i)
            ];

            $rows[] = implode(',', $dataForRow);
        }

        $content = implode("\n", $rows);

        $response = new Response($content);

        // set headers
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', "attachment; filename=$year-salary-dates.csv");

        // return download
        return $response;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function plainText($year, $data)
    {
        $rows = [];

        // paste data into CSV
        $max = sizeof($data->months);
        for ($i = 0; $i < $max; $i++) {

            $dataForRow = [
                $data->months[$i] . ":\n---------\nSalary date: ",
                ReportController::salary($year, $i) . " \nBonus date: ",
                ReportController::bonus($year, $i) . "\n",
            ];

            $rows[] = implode($dataForRow);
        }

        $content = implode("\n", $rows);

        $response = new Response($content);

        // set headers
        $response->headers->set('Content-Type', 'text/plain');
        $response->headers->set('Content-Disposition', "attachment; filename=$year-salary-dates.txt");

        // return download
        return $response;
    }

    private function months(&$data)
    {
        for ($i = 1; $i <= 12; $i++) {
            array_push($data->months, strftime('%B', mktime(0, 0, 0, $i, 10)));
            // dump(strftime('%B', mktime(0, 0, 0, $i, 10)));
        }
    }

    private function salary($year, $i)
    {
        // last day of month (if weekday, else previous friday end of week)
        $firstDay = $year  . "-" . (string) $i . "-01";
        $date = new DateTime($firstDay);
        $salaryDay = $date->format('Y-m-t');

        if (ReportController::checkIsWeekend($salaryDay)) {
            $salaryDay = date('Y-m-d', strtotime('previous friday', strtotime($salaryDay)));
        }
        return $salaryDay;
    }

    private function bonus($year, $i)
    {
        $date =   strtotime($year  . "-" . (string) $i . "-15");
        $bonusDay = date("Y-m-d", strtotime('+1 months', $date));

        if (ReportController::checkIsWeekend($bonusDay)) {
            $bonusDay = date('Y-m-d', strtotime('next wednesday', strtotime($bonusDay)));
        }
        return $bonusDay;
    }

    private function checkIsWeekend($date)
    {
        $dayOfWeek = date('w', strtotime($date));
        return ($dayOfWeek == 0 || $dayOfWeek == 6);
    }

    private function setLanguage($language)
    {
        switch ($language) {
            case 'nl':
                setlocale(LC_ALL, $language, 'Dutch_Belgium',  'nl_BE', 'nl_BE@euro');
                break;

            default:
                // not specified language (such as french)
                setlocale(LC_ALL, $language);
                break;
        }
    }
}