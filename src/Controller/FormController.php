<?php

namespace App\Controller;

use App\Controller\ReportController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FormController extends AbstractController
{
    /**
     * @Route("/", name="form")
     */
    public function index(Request $request)
    {
        $defaultData = ['year' => date("Y")];

        $form = $this->createFormBuilder($defaultData)
            ->add(
                'year',
                NumberType::class,
                [
                    'html5' => true, 'help' => 'Van welk jaar wil je de gegevens opvragen?', 'label' => 'Jaar',
                    'attr' => ['min' => 100, 'max' => 9998]
                ]
            )
            ->add('choiceLanguage', ChoiceType::class, [
                'choices'  =>
                [
                    'Engels' => null,
                    'Nederlands' => 'nl',
                    'Frans' => 'fr'
                ],
                'data' => substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2), // get browser language and set active if this is one of the choices
                'label' => 'Taal voor maanden'
            ])
            ->add('plainText', CheckboxType::class, ['label' => 'Plain text', 'required' => false])
            ->add('save', SubmitType::class, ['label' => 'Verstuur'])
            ->getForm();

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            return ReportController::generate($data);
        }

        return $this->render('form/index.html.twig', [
            'title' => 'Mikko',
            'form' => $form->createView()
        ]);
    }
}